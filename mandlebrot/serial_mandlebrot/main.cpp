#include<stdio.h>
#include<math.h>
#include <iostream>
#include <fstream>
#include <limits>
#include<omp.h>

#define X 2000 // X pixel count
#define Y 2000 // Y ppixel count
#define BRIGHTNESS 255
#define GRID 1.5
#define MAX_RECURSION 300 
#define DETAIL 150

// Camera position
// Positive value moves the view to the right/down.
// Negative value moves the view to the left/up
#define X_VIEW -0.6 
#define Y_VIEW 0

std::ofstream OutFile;

int map_int(double y_min, double y_max, double x_min, double x_max, double x_convert)
{
	double slope = ( (y_max - y_min) / (x_max - x_min) );
	return (int)(slope * x_convert);
}

double map_double(double y_min, double y_max, double x_min, double x_max, double x_convert)
{
	double slope = ( (y_max - y_min) / (x_max - x_min) );
	return (slope * x_convert);
}

int main()
{
	double x_index = 0, y_index = 0;
	double a = 0, b = 0;
	int recursion = 0;

	// next values of a and b.
	double next_a = 0;
	double next_b = 0;

	// Original values of a and b.
	double a_still = 0;
	double b_still = 0;

	// Opem file and set pgm file.
	OutFile.open ("out.pgm");
	OutFile << "P2\n";
	OutFile << "# Mandlebrot output image. \n";
	OutFile << X << " " <<  Y << "\n";
	OutFile << MAX_RECURSION <<" \n";

	// Timing variables
	double exec_time;
	double start = omp_get_wtime() ;

	for(y_index = 0; y_index < Y; y_index++)
	{
		for(x_index = 0; x_index < X; x_index++)
		{
			// Convert indexes into grid values.
			a = map_double(0, (GRID * 2), 0, X, x_index) - GRID + X_VIEW;
			b = map_double(0, (GRID * 2), 0, Y, y_index) - GRID + Y_VIEW;	
			//a = x_index - X/2;
			//b = y_index - Y/2;

			a_still = a;
			b_still = b;

			next_a = 0;
			next_b = 0;

			recursion = 0;

			// Escape loop if determined infinite
			while(recursion < MAX_RECURSION)
			{
				next_a  = (a * a) - (b * b);
				next_b  = 2*a*b;

				a = next_a + a_still;			
				b = next_b + b_still;

				// Escape if not infinite.
				//if((a + b) > 16)
				if(((a * a) + (b *b)) > DETAIL)
				{
					break;
				}

				recursion++;	
			}
			
			//if(recursion == MAX_RECURSION) OutFile << "0 ";
			//else OutFile << map_int(0, BRIGHTNESS, 0, MAX_RECURSION, recursion)<< " ";	
		}
		//OutFile << "\n";	 
	}

	// End timer.
	double end = omp_get_wtime();
	exec_time = end - start;
	printf("TIME : The run time is %15.15f \n", exec_time);

	FILE *log = fopen("log.txt","a");
	fprintf(log, "%f,\n", exec_time);
	fclose(log);


	OutFile.close();
	return 0;
}





