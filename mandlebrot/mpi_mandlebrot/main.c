#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <mpi.h>

#define CLUSTER_COUNT 5
#define SLAVE_COUNT 4 // minus one from the CLUSTER_COUNT
#define X 2000 // X pixel count
#define Y 2000 // Y ppixel count
#define BRIGHTNESS 255
#define GRID 1.5
#define MAX_RECURSION 300 
#define DETAIL 150

// Camera position
// Positive value moves the view to the right/down.
// Negative value moves the view to the left/up
#define X_VIEW -0.6 
#define Y_VIEW 0

FILE *OutFile;

int map_int(double y_min, double y_max, double x_min, double x_max, double x_convert)
{
	double slope = ( (y_max - y_min) / (x_max - x_min) );
	return (int)(slope * x_convert);
}

double map_double(double y_min, double y_max, double x_min, double x_max, double x_convert)
{
	double slope = ( (y_max - y_min) / (x_max - x_min) );
	return (slope * x_convert);
}

int main()
{
	// Initialize MPI
	MPI_Status status;
	int numprocs;
	int myid;
	MPI_Init (NULL , NULL);
	MPI_Comm_size ( MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank ( MPI_COMM_WORLD, &myid);

	// Array index.
	int x_in = 0, y_in = 0;

	// Open file and set pgm file.
	OutFile = fopen("out.pgm", "w");
        fprintf(OutFile, "P2\n");
	fprintf(OutFile, "# Mandlebrot output image. \n");
	fprintf(OutFile, "%d %d\n", X, Y);
	fprintf(OutFile, "%d\n", MAX_RECURSION);

	// Slave clusters.
	if(myid != 0)
	{
		// Slave ID
		int slave_id = myid - 1;

		// Partition buffer size of each cluster.
		int partition = Y/SLAVE_COUNT;

		// buffer array.
		int buffer[X][Y/SLAVE_COUNT] = { 0 };

		// Misc. variables.
		int x_index = 0, y_index = 0;
		double a = 0, b = 0;
		int recursion = 0;

		// next values of a and b.
		double next_a = 0;
		double next_b = 0;

		// Original values of a and b.
		double a_still = 0;
		double b_still = 0;

		for(y_index = (slave_id * partition); y_index < (partition * (slave_id + 1)); y_index++)
		{
			for(x_index = 0; x_index < X; x_index++)
			{

				// Convert indexes into grid values.
				a = map_double(0, (GRID * 2), 0, X, (double)x_index) - GRID + X_VIEW;
				b = map_double(0, (GRID * 2), 0, Y, (double)y_index) - GRID + Y_VIEW;	

				a_still = a;
				b_still = b;

				next_a = 0;
				next_b = 0;

				recursion = 0;

				// Escape loop if determined infinite
				while(recursion < MAX_RECURSION)
				{
					next_a  = (a * a) - (b * b);
					next_b  = 2*a*b;

					a = next_a + a_still;
					b = next_b + b_still;

					if(((a * a) + (b *b)) > DETAIL)
					{
						break;
					}

					recursion++;
				}

				if(recursion == MAX_RECURSION)
				{
					buffer[x_index][y_index] = 0;
				}
				else
				{
					buffer[x_index][y_index] = map_int(0, BRIGHTNESS, 0, MAX_RECURSION, recursion);
				}
			}
		}

		printf("NODE %d : Finished calculating mandlebrot.\n", myid);
		MPI_Send(buffer, X*partition , MPI_INT, 0, 0, MPI_COMM_WORLD);
	}
	// Master cluster operation.
	else
	{
		// Start Timer
		double exec_time;
		double start = omp_get_wtime() ;

		printf("MASTER : Starting data acquisition...\n");

		// Master's buffer.
		int master_buffer[X][Y/SLAVE_COUNT] = { 0 };

		// Receeive all slave data.
		int id_iterate = 0;
		for(id_iterate = 0; id_iterate < SLAVE_COUNT;id_iterate++)
		{
			// Get data from slave nodes.
			MPI_Recv(master_buffer, (X * (Y/SLAVE_COUNT)), MPI_INT, id_iterate + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			//printf("MASTER : getting data form node %d.\n", id_iterate + 1);
/*
			// Write exrtracted data to the output file.
			for(y_in = 0; y_in <(Y/SLAVE_COUNT); y_in++)
			{
				for(x_in = 0; x_in < X; x_in++)
				{
					fprintf(OutFile, "%d ", master_buffer[x_in][y_in]);
				}
				fprintf(OutFile, "\n");
			}*/
		}

		// End timer.
		double end = omp_get_wtime();
		exec_time = end - start;
		printf("TIME : The run time is %15.15f\n", exec_time);

		FILE *log = fopen("log.txt", "a");
		fprintf(log, "%f,\n", exec_time);
	}

	// Close everything.
	fclose(OutFile);
	MPI_Finalize();
	return 0;
}

