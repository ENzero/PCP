ABOUT
============================================
This is directory contains the implementation of mandlebrot using MPI.

Compile and Run
============================================
This project is advised to be ran in a proper cluster envirnoment.
Here are the steps to compile and run the program:
- make clean
- make
- mpirun -np 5 mpi_out
