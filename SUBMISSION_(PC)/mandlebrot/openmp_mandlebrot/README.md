ABOUT
============================================
This is directory contains the implementation of mandlebrot using MPI.

Installing OpenMP
============================================
- $ sudo apt-get install gcc-multilib

Compile and Run
============================================
Here is the step to compile and run the program:
- g++  -fopenmp main.cpp
- ./a.out
