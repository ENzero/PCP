/****************************************************************************************/
/* 			OPENMP MANDELBROT MAIN PROGRAM		         		*/
/****************************************************************************************/
/* Name : main.cpp									*/
/* Author : Angelo Charl Sison								*/
/* Student Number : 14037885								*/
/* Created Date : 2/3/2018								*/
/* Project Name : serial_mandelbrot							*/
/* Description : This file contains the code for a OpenMP implementation of the		*/
/*		 mandelbrot algorithm. The program producess an image file of the   	*/
/*		 mandelbrot image.							*/
/* User Advice : This program is programmed to work on Debian-based OS.			*/
/****************************************************************************************/
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <limits>
#include <omp.h>
#include <vector>

#define NUM_THREADS 4
#define X 2000 // X pixel count
#define Y 2000 // Y ppixel count
#define BRIGHTNESS 255
#define GRID 1.5
#define MAX_RECURSION 300 
#define DETAIL 150

// Camera position
// Positive value moves the view to the right/down.
// Negative value moves the view to the left/up
#define X_VIEW -0.6 
#define Y_VIEW 0

using namespace std;

// File stream for output image file.
std::ofstream OutFile;

/********************************************************************************/
/* Function name : int map_int(double y_min, double y_max, 			*/
/*			double x_min, double x_max, double x_convert)		*/
/* - returns : The scaled value int INT.					*/
/* - y_min : Minimum range of the output scale.					*/
/* - y_max : maximum range of the output scale.					*/
/* - x_min : Minimum range of the input scale.					*/
/* - x_max : Maximum range of the input scale.					*/
/* - x_convert : Value to be scaled.						*/
/* Created by : Angelo Charl Sison						*/
/* Date Created : 2/3/2018							*/
/* Description : This function scales a value based on the output range in INT.	*/
/*		 Linear formual is used.					*/
/********************************************************************************/
int map_int(double y_min, double y_max, double x_min, double x_max, double x_convert)
{
	double slope = ( (y_max - y_min) / (x_max - x_min) );
	return (int)(slope * x_convert);
}

/********************************************************************************/
/* Function name : map_double map_double(double y_min, double y_max, 		*/
/*			double x_min, double x_max, double x_convert)		*/
/* - returns : The scaled value in DOUBLE.					*/
/* - y_min : Minimum range of the output scale.					*/
/* - y_max : maximum range of the output scale.					*/
/* - x_min : Minimum range of the input scale.					*/
/* - x_max : Maximum range of the input scale.					*/
/* - x_convert : Value to be scaled.						*/
/* Created by : Angelo Charl Sison						*/
/* Date Created : 2/3/2018							*/
/* Description : This function scales a value based on the output range in	*/
/*		 DOUBLE. Linear formual is used.				*/
/********************************************************************************/
double map_double(double y_min, double y_max, double x_min, double x_max, double x_convert)
{
	double slope = ( (y_max - y_min) / (x_max - x_min) );
	return (slope * x_convert);
}

/********************************************************************************/
/* Function name : int main()							*/
/* - returns : The termination code.						*/
/* Created by : Angelo Charl Sison						*/
/* Date Created : 2/3/2018							*/
/* Description : This function contains the main implementation.		*/
/********************************************************************************/
int main()
{
	// Array index.
	int x_in = 0, y_in = 0;

	// Opem file and set pgm file.
	OutFile.open ("out.pgm");
	OutFile << "P2\n";
	OutFile << "# Mandlebrot output image. \n";
	OutFile << X << " " <<  Y << "\n";
	OutFile << MAX_RECURSION <<" \n";

	// Pixel qurter size count.
	int quarter = (Y / NUM_THREADS);

	// Initialize buffer.
	vector< vector<int> > buffer;
	vector<int> init;
	for(y_in = 0; y_in < Y; y_in++)
	{
		buffer.push_back(init);
		for(x_in = 0; x_in < X; x_in++)
		{
			buffer[(buffer.size() -1)].push_back(0);
		}
	}

	// Start Timer
	double exec_time;
	double start = omp_get_wtime() ;

	// Start parallelization.
	#pragma omp parallel
	{
		// Get the ID of the process.
		int id = omp_get_thread_num();

		// Temporary variables for index and recursion values.
		int x_index = 0, y_index = 0;
		double a = 0, b = 0;
		int recursion = 0;

		// next values of a and b.
		double next_a = 0;
		double next_b = 0;

		// Original values of a and b.
		double a_still = 0;
		double b_still = 0;
		
		// Split workload between processes through y-axis.
		for(y_index = (id * quarter); y_index < (quarter*(id+1)); y_index++)
		{
			for(x_index = 0; x_index < X; x_index++)
			{
				// Convert indexes into grid values.
				a = map_double(0, (GRID * 2), 0, X, (double)x_index) - GRID + X_VIEW;
				b = map_double(0, (GRID * 2), 0, Y, (double)y_index) - GRID + Y_VIEW;	

				a_still = a;
				b_still = b;

				next_a = 0;
				next_b = 0;

				recursion = 0;

				// Escape loop if determined infinite
				while(recursion < MAX_RECURSION)
				{
					next_a  = (a * a) - (b * b);
					next_b  = 2*a*b;

					a = next_a + a_still;			
					b = next_b + b_still;

					if(((a * a) + (b *b)) > DETAIL)
					{
						break;
					}

					recursion++;	
				}
			
				// Set pixel color.
				if(recursion == MAX_RECURSION) 
				{
					buffer[x_index][y_index] = 0;
				}
				else
				{ 
					buffer[x_index][y_index] = map_int(0, BRIGHTNESS, 0, MAX_RECURSION, recursion);
				}
			}
		}
	}

	// Write pixels to the output file.
	for(y_in = 0; y_in <Y; y_in++)
	{
		for(x_in = 0; x_in < X; x_in++)
		{
			OutFile << buffer[x_in][y_in] << " ";
		}
		OutFile << std::endl;
	}

	// End timer.
	double end = omp_get_wtime();
	exec_time = end - start;
	printf("TIME : The run time is %15.15f \n", exec_time);

	// Close file stream.
	OutFile.close();

	return 0;
}

