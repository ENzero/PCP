#include<stdio.h>
#include<math.h>
#include <iostream>
#include <fstream>
#include <limits>
#include<omp.h>

#define NUM_THREADS 4
#define X 6000 // X pixel count
#define Y 6000 // Y ppixel count
#define BRIGHTNESS 255
#define GRID 1.5
#define MAX_RECURSION 300 
#define DETAIL 150

// Camera position
// Positive value moves the view to the right/down.
// Negative value moves the view to the left/up
#define X_VIEW -0.6 
#define Y_VIEW 0

std::ofstream OutFile;

int map_int(double y_min, double y_max, double x_min, double x_max, double x_convert)
{
	double slope = ( (y_max - y_min) / (x_max - x_min) );
	return (int)(slope * x_convert);
}

double map_double(double y_min, double y_max, double x_min, double x_max, double x_convert)
{
	double slope = ( (y_max - y_min) / (x_max - x_min) );
	return (slope * x_convert);
}

int main()
{
	double a = 0, b = 0;
	int recursion = 0;

	// next values of a and b.
	double next_a = 0;
	double next_b = 0;

	// Original values of a and b.
	double a_still = 0;
	double b_still = 0;

	// Parallel files
	std::ofstream parallelImageStream[NUM_THREADS];
	parallelImageStream[0].open ("p0.txt");
	parallelImageStream[1].open ("p1.txt");
	parallelImageStream[2].open ("p2.txt");
	parallelImageStream[3].open ("p3.txt");

	// Opem file and set pgm file.
	OutFile.open ("out.pgm");
	OutFile << "P2\n";
	OutFile << "# Mandlebrot output image. \n";
	OutFile << X << " " <<  Y << "\n";
	OutFile << MAX_RECURSION <<" \n";

	// Timing variables
	double exec_time;
	double start = omp_get_wtime() ;

	#pragma omp parallel
	{
		int id = omp_get_thread_num();
		int x_index = 0, y_index = 0;
		int quarter = (Y / NUM_THREADS);

		#pragma omp for
		for(y_index = (id * quarter); y_index < (quarter*(id+1)); y_index++)
		{
			for(x_index = 0; x_index < X; x_index++)
			{
				// Convert indexes into grid values.
				a = map_double(0, (GRID * 2), 0, X, (double)x_index) - GRID + X_VIEW;
				b = map_double(0, (GRID * 2), 0, Y, (double)y_index) - GRID + Y_VIEW;	

				a_still = a;
				b_still = b;

				next_a = 0;
				next_b = 0;

				recursion = 0;

				// Escape loop if determined infinite
				while(recursion < MAX_RECURSION)
				{
					next_a  = (a * a) - (b * b);
					next_b  = 2*a*b;

					a = next_a + a_still;			
					b = next_b + b_still;

					if(((a * a) + (b *b)) > DETAIL)
					{
						break;
					}

					recursion++;	
				}
			
				if(recursion == MAX_RECURSION) parallelImageStream[id] << "0 ";
				else parallelImageStream[id]  << map_int(0, BRIGHTNESS, 0, MAX_RECURSION, recursion)<< " ";	
			}
			parallelImageStream[id]  << "\n";	 
		}
	}

	double end = omp_get_wtime();
	exec_time = end - start;
	printf("the time difference is %15.15f", exec_time);

	OutFile.close();	

	parallelImageStream[0].close();
	parallelImageStream[1].close();
	parallelImageStream[2].close();
	parallelImageStream[3].close();

	return 0;
}





